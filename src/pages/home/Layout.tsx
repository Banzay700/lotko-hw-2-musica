import { Component } from 'react'
import { Header } from 'src/components'
import { SharedTypes, Product } from 'src/types/productTypes'
import { ProductList } from './product-list'
import s from './Home.module.scss'

class Layout extends Component<any, SharedTypes> {
  state = {
    favorites: [],
    cart: [],
  }

  setFavouriteProduct = (product: Product) => {
    const { favorites } = this.state
    const isMatches = favorites.some((item: Product) => item.id === product.id)

    if (isMatches) {
      const filteredProducts = favorites.filter((item: Product) => item.id !== product.id)

      this.setState({ favorites: filteredProducts })
    } else {
      this.setState((prv: SharedTypes) => ({ favorites: [...prv.favorites, product] }))
    }
  }

  buyProduct = (product: Product) => {
    this.setState((prv: SharedTypes) => ({ cart: [...prv.cart, product] }))
  }

  render() {
    const { favorites, cart } = this.state

    return (
      <div className={s.layout}>
        <Header favorites={favorites} cart={cart} />
        <ProductList setFavouriteProduct={this.setFavouriteProduct} buyProduct={this.buyProduct} />
      </div>
    )
  }
}

export default Layout
