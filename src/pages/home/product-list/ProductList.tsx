import { Component } from 'react'
import { ProductCard } from 'src/components'
import { Modal } from 'src/UI'
import { Product } from 'src/types/productTypes'
import 'src/data/products.json'
import s from './ProductList.module.scss'

interface ProductListProps {
  setFavouriteProduct: (product: Product) => void
  buyProduct: (product: Product) => void
}

interface ProductListState {
  productToBuy: Product
  products: Product[]
  isOpenedModal: boolean
}

class ProductList extends Component<ProductListProps, ProductListState> {
  state = {
    productToBuy: {} as Product,
    products: [],
    isOpenedModal: false,
  }

  async componentDidMount() {
    const request = await fetch('src/data/products.json')
    const data = await request.json()

    this.setState({ products: data })
  }

  setProductToBuy = (selectedProduct: Product) => {
    this.setState({ productToBuy: selectedProduct, isOpenedModal: true })
  }

  closeModal = () => {
    this.setState({ isOpenedModal: false })
  }

  render() {
    const { setFavouriteProduct, buyProduct } = this.props
    const { products, isOpenedModal, productToBuy } = this.state

    return (
      <>
        <div className={s.title}>Our Products</div>
        {isOpenedModal && (
          <Modal selectProduct={productToBuy} onClose={this.closeModal} buyProduct={buyProduct} />
        )}
        <div className={s.list}>
          {products.map((item: Product) => (
            <ProductCard
              data={item}
              key={item.id}
              setFavouriteProduct={setFavouriteProduct}
              setProductToBuy={this.setProductToBuy}
            />
          ))}
        </div>
      </>
    )
  }
}

export default ProductList
