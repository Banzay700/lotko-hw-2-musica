export interface Product {
  id: number
  title: string
  price: number
  thumbnail: string
}

export interface SharedTypes {
  favorites: Product[]
  cart: Product[]
}
