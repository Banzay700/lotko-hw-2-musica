import { Component } from 'react'
import { CartButton, LikeButton } from 'src/UI'
import { SharedTypes } from 'src/types/productTypes'
import s from './Header.module.scss'

class Header extends Component<SharedTypes> {
  render() {
    const { favorites, cart } = this.props

    return (
      <div className={s.header}>
        <div className={s.logo}>Logo</div>
        <div className={s.navMenu}>Home</div>
        <div className={s.actions}>
          <div className={s.headerBtnWrapper}>
            <LikeButton />
            <span className={s.counter}>{favorites.length}</span>
          </div>
          <div className={s.headerBtnWrapper}>
            <CartButton />
            <span className={s.counter}>{cart.length}</span>
          </div>
        </div>
      </div>
    )
  }
}

export default Header
