import { Component } from 'react'
import { LikeButton, CartButton } from 'src/UI'
import s from './ProductCard.module.scss'

class ProductCard extends Component<any, any> {
  render() {
    const { data, setFavouriteProduct, setProductToBuy } = this.props

    return (
      <div className={s.product}>
        <div>{data.title}</div>
        <div className={s.image}>
          <img src={data.thumbnail} alt="product" />
        </div>
        <div className={s.article}>product #: {data.stock}</div>
        <div>&#36; {data.price}</div>
        <div className={s.actions}>
          <LikeButton onClick={() => setFavouriteProduct(data)} />
          <CartButton onClick={() => setProductToBuy(data)} />
        </div>
      </div>
    )
  }
}

export default ProductCard
