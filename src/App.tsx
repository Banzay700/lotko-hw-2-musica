import { Component } from 'react'
import { Layout } from './pages'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout />
      </div>
    )
  }
}

export default App
