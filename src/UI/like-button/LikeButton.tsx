import { Component } from 'react'
import cn from 'classnames'
import { motion } from 'framer-motion'
import { DefaultButtonProps } from 'src/types/buttonTypes'
import s from './LikeButton.module.scss'

class LikeButton extends Component<DefaultButtonProps> {
  state = {
    likeBtnStyle: s.likeIcon,
  }

  setBtnStyle = () => {
    const { likeBtnStyle } = this.state
    const btnStyle = likeBtnStyle === s.likeIcon ? s.iconActive : s.likeIcon

    this.setState({ likeBtnStyle: btnStyle })
  }

  render() {
    const { likeBtnStyle } = this.state
    const { onClick } = this.props

    return (
      <motion.div whileTap={{ scale: 1.2 }} onClick={onClick}>
        <i onClick={this.setBtnStyle} className={cn('ri-heart-2-fill', likeBtnStyle)} />
      </motion.div>
    )
  }
}

export default LikeButton
