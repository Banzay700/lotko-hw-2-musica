import { Component } from 'react'
import cn from 'classnames'
import { DefaultButtonProps } from 'src/types/buttonTypes'
import s from './Button.module.scss'

interface ButtonProps extends DefaultButtonProps {
  text: string
  style: string
}

class Button extends Component<ButtonProps> {
  render() {
    const { text, style, onClick } = this.props

    return (
      <button className={cn(s.button, style)} onClick={onClick} type="button">
        {text}
      </button>
    )
  }
}

export default Button
