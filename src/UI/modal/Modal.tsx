import React, { Component } from 'react'
import cn from 'classnames'
import { Button } from 'src/UI'
import { Product } from 'src/types/productTypes'
import s from './Modal.module.scss'

interface ModalProps {
  selectProduct: Product
  onClose: () => void
  buyProduct: (selectProduct: Product) => void
}

class Modal extends Component<ModalProps> {
  render() {
    const { selectProduct, onClose, buyProduct } = this.props

    const completePurchase = () => {
      onClose()
      buyProduct(selectProduct)
    }

    return (
      <>
        <div className={s.overlay} onClick={onClose} />
        <div className={s.modal}>
          <i className={cn('ri-close-fill', s.close)} onClick={onClose} />
          <div className={s.heading}>{selectProduct.title}</div>
          <div className={s.modalContent}>
            <div className={s.productImage}>
              <img src={selectProduct.thumbnail} alt="Product" />
            </div>
            <div className={s.productInfo}>
              <div>Price: &#36;{selectProduct.price}</div>
              <div>quantity 1 </div>
              <div>Total cost: {selectProduct.price}</div>
            </div>
          </div>
          <Button style={s.modalButton} onClick={completePurchase} text="ADD TO BAG" />
        </div>
      </>
    )
  }
}

export default Modal
