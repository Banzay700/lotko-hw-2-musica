import { Component } from 'react'
import { motion } from 'framer-motion'
import cn from 'classnames'
import { DefaultButtonProps } from 'src/types/buttonTypes'

import cartIcon from './CartButton.module.scss'

class CartButton extends Component<DefaultButtonProps> {
  render() {
    const { onClick } = this.props

    return (
      <motion.div whileTap={{ scale: 1.2 }} onClick={onClick}>
        <i className={cn('ri-shopping-cart-2-fill', cartIcon)} />
      </motion.div>
    )
  }
}

export default CartButton
